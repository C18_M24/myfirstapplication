package uk.ac.tees.w9493268.bestprice;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
@GET("posts")
Call<List<PostPojo>> getposts();
}
